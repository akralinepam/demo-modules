// Global Variables
variable "artifact_bucket" {
  type        = string
  description = "Bucket with lambda, glue artifacts and dataproduct definitions described in tf files"
}

variable "artifacts_version" {
  type        = string
  default     = "current"
  description = "Dataproduct version. Used for glue, lambda artifacts and tf file version related to dataproduct definitions"
}

variable "secondary_artifact_version" {
  type        = map
  default     = {}
  description = "Map with all dataproduct versions"
}

variable "iam_path" {
  type        = string
  description = "Default IAM path. User for roles & policies created by terraform."
}

variable "aws_region" {
  type        = string
  description = "Used for AWS provider"
}

variable "environment" {
  type        = string
  description = "Environment"
}

// TFState related variables
variable "tfstate_v2_s3_bucket" {
  type        = string
  description = "Bucket with state file used in architecture 2.0"
}

variable "tfstate_lake_key_prefix" {
  type        = string
  description = "TFstate base path used for dataproducts"
}

variable "tags" {
  type        = map(string)
  description = "Default tags"
  default     = {}
}

variable "local_tags" {
  type        = map(string)
  description = "Tags specific for current module"
  default     = {}
}

variable "preserve_data_envs" {
  type        = list(string)
  description = "List of enviroments with preserved data daletion option"
  default     = ["prod"]
}

variable "valid_lambda_identifiers" {
  type        = list(string)
  description = "List of valid lambda identifiers"
  default     = ["python3.8", "python3.7", "python3.6", "java8", "go1.x"]
}

variable "valid_glue_identifiers" {
  type        = list(string)
  description = "List of valid glue identifiers"
  default     = ["pythonshell", "glueetl"]
}

variable "default_glue_job_args" {
  type        = map(string)
  description = "Map of default options for glue etl jobs"
  default = {
    "--enable-metrics" = ""
  }
}

variable "default_s3_sse_algorithm" {
  type        = string
  description = "S3 default encryption algorithm"
  default     = "AES256"
}

variable "default_s3_versioning_enabled" {
  type        = bool
  description = "S3 versioning enabled"
  default     = false
}

variable "valid_environments" {
  type        = list(string)
  description = "List of valid environments. This is used when we decide should we add some env suffix for resource on not"
  default     = ["dev", "test", "prod", "qa"]
}

/*
  List of suffixes used for different AWS resources
*/

variable "default_glue_version" {
  type        = string
  description = "Default glue version to use"
  default     = null
}

variable "default_dynamodb_read_capacity" {
  type        = string
  description = "Default dynamodb read capacity"
  default     = 3
}

variable "default_dynamodb_write_capacity" {
  type        = string
  description = "Default dynamodb write capacity"
  default     = 3
}

variable "default_dynamodb_billing_mode" {
  type        = string
  description = "Default dynamodb billing mode"
  default     = "PROVISIONED"
}

variable "default_s3_ro_actions" {
  type = list(string)
  default = [
    "s3:GetObject",
    "s3:GetObjectTagging",
    "s3:GetObjectVersion",
    "s3:GetObjectVersionTagging",
    "s3:ListMultipartUploadParts",
    # Bucket level
    "s3:ListBucket",
    "s3:ListBucketMultipartUploads",
    "s3:GetBucketLocation",
    "s3:GetBucketTagging",
  "s3:GetBucketVersioning"]
}

variable "default_s3_wo_actions" {
  type = list(string)
  default = [
    "s3:DeleteObject",
    "s3:DeleteObjectTagging",
    "s3:DeleteObjectVersion",
    "s3:DeleteObjectVersionTagging",
    "s3:AbortMultipartUpload",
    "s3:PutObject",
    "s3:PutObjectTagging",
    "s3:PutObjectVersionTagging",
  "s3:RestoreObject"]
}

variable "default_dynamodb_wo" {
  type = list(string)
  default = [
    "dynamodb:BatchWriteItem",
    "dynamodb:DeleteItem",
    "dynamodb:PutItem",
    "dynamodb:UpdateItem",
  ]
}
variable "default_dynamodb_ro" {
  type = list(string)
  default = [
    "dynamodb:BatchGetItem",
    "dynamodb:ConditionCheckItem",
    "dynamodb:DescribeTimeToLive",
    "dynamodb:DescribeTable",
    "dynamodb:GetItem",
    "dynamodb:GetRecords",
    "dynamodb:GetShardIterator",
    "dynamodb:Query",
    "dynamodb:Scan",
  ]
}

variable "lifecycle_rule" {
  description = "List of maps containing configuration of object lifecycle management."
  type        = any
  default     = []
}
