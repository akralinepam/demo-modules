variable "subscribe_on" {
  type        = list(string)
  description = "List of lower layer dataproduct on which current dataproduct depends on"
  default     = []
}


locals {
  helpers = {
    // here is well known helpers
  }

  /*
  Dataproduct name, this name will be inherited of all dependend resources
  create for this dataproduct, like: s3, sns, sqs, dynamodb .... */
  name = ""
  /*
  Dataproduct description, will be added to glue database as a description */
  description = ""
  /*
  Enable dynamodb metadata storage */
  dynamodb_enabled = true
  /*
  Enable crawler orchestration as part of StepFunction flow */
  crawler_enabled = true
  crawler_paths   = []
  /*

  /*
  Enable automatic database creation with the same name as name attribute */
  create_database = true

  s3_notification_configuration = []
  /*
    If you need some precreated folder inside our bucket
    put this list here. Note: all folders should have "/" character at the end
  */
  s3_folder_structure = []

  s3_objects = []

}
