// Get account data
data "aws_caller_identity" "this" {}
data "aws_region" "this" {}

## Begin: remote state section
/*
  terraform_remote_state.this - is responsible to fetch all resources from lower layer products that
  described in sections subscribe_on and rw_buckets.
  The main differencies between this two list is the following:
    - subscribe_on - subscribed on lower layer products and generate policy to make this
      dataproducts available in readonly mode;
    - rw_buckets - products are described here are available in rw-mode. No subscriptions are created.

  Note: to make developers live easier I also create aliases for resource attributes in locals section in main.tf
*/
data "terraform_remote_state" "this" {
  for_each = toset(compact(concat(var.subscribe_on)))
  backend  = "s3"
  config = {
    bucket = var.tfstate_v2_s3_bucket
    key    = format("%s/%s/terraform.tfstate", var.tfstate_lake_key_prefix, each.value)
    region = var.aws_region
  }
}
