##################################
# Locals
##################################
locals {
  /* Validate input parameters
    if some of parameters bellow is not defined module should explicelty failed. */
  assert_environment_variable_not_set = var.environment == "" ? file("ERROR: \"environment\" variable is not set") : null
  assert_local_artifact_bucket        = var.artifact_bucket == "" ? file("ERROR: \"artifact_bucket\" variable is not set") : null
  assert_local_iam_path               = var.iam_path == "" ? file("ERROR: \"iam_path\" variable is not set") : null
  assert_aws_region                   = var.aws_region == "" ? file("ERROR: \"aws_region\" variable is not set") : null

  // Effective tags for resources
  effective_tags = merge(var.tags, var.local_tags)

  // Remote state aliases
  tfstate_dp          = { for key, value in data.terraform_remote_state.this : key => value.outputs }
}

##################################
# Generate random ID
##################################
// As some resources should be uniq across all AWS accounts or within one AWS account
// we add some random salt to it.
resource "random_string" "this" {
  length      = 6
  special     = false
  upper       = false
  number      = true
  lower       = true
  min_numeric = 3
  min_lower   = 3
}

##################################
# Storage
##################################
resource "aws_s3_bucket" "this" {
  bucket = join("-", [local.name, var.environment, random_string.this.result])
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = var.default_s3_sse_algorithm
      }
    }
  }
  versioning {
    enabled = var.default_s3_versioning_enabled
  }
  force_destroy = contains(var.preserve_data_envs, var.environment) ? false : true
  tags          = local.effective_tags

  # lifecycle was copied from terraform-aws-modules/terraform-aws-s3-bucket
  #  https://github.com/terraform-aws-modules/terraform-aws-s3-bucket/blob/a21f43c5e1befd69e564a8fc8c7d02094137e9db/main.tf#L64
  dynamic "lifecycle_rule" {
    for_each = var.lifecycle_rule

    content {
      id                                     = lookup(lifecycle_rule.value, "id", null)
      prefix                                 = lookup(lifecycle_rule.value, "prefix", null)
      tags                                   = lookup(lifecycle_rule.value, "tags", null)
      abort_incomplete_multipart_upload_days = lookup(lifecycle_rule.value, "abort_incomplete_multipart_upload_days", null)
      enabled                                = lifecycle_rule.value.enabled

      # Max 1 block - expiration
      dynamic "expiration" {
        for_each = length(keys(lookup(lifecycle_rule.value, "expiration", {}))) == 0 ? [] : [lookup(lifecycle_rule.value, "expiration", {})]

        content {
          date                         = lookup(expiration.value, "date", null)
          days                         = lookup(expiration.value, "days", null)
          expired_object_delete_marker = lookup(expiration.value, "expired_object_delete_marker", null)
        }
      }

      # Several blocks - transition
      dynamic "transition" {
        for_each = lookup(lifecycle_rule.value, "transition", [])

        content {
          date          = lookup(transition.value, "date", null)
          days          = lookup(transition.value, "days", null)
          storage_class = transition.value.storage_class
        }
      }

      # Max 1 block - noncurrent_version_expiration
      dynamic "noncurrent_version_expiration" {
        for_each = length(keys(lookup(lifecycle_rule.value, "noncurrent_version_expiration", {}))) == 0 ? [] : [lookup(lifecycle_rule.value, "noncurrent_version_expiration", {})]

        content {
          days = lookup(noncurrent_version_expiration.value, "days", null)
        }
      }

      # Several blocks - noncurrent_version_transition
      dynamic "noncurrent_version_transition" {
        for_each = lookup(lifecycle_rule.value, "noncurrent_version_transition", [])

        content {
          days          = lookup(noncurrent_version_transition.value, "days", null)
          storage_class = noncurrent_version_transition.value.storage_class
        }
      }
    }
  }
}



// Sometimes it's very usefull to have some predifined folder structure for S3 buckets
// for example it can be used for landing zone
resource "aws_s3_bucket_object" "this" {
  count  = length(local.s3_folder_structure)
  bucket = aws_s3_bucket.this.id
  key    = local.s3_folder_structure[count.index]
}

// Put some predefined objects to S3 bucket
resource "aws_s3_bucket_object" "file" {
  count          = length(local.s3_objects)
  bucket         = aws_s3_bucket.this.id
  key            = local.s3_objects[count.index].key
  content_base64 = local.s3_objects[count.index].content
}



##################################
# Dynamodb related resources
##################################
/*
  This dynamodb database is handle all metadata information related to
  orchestration workflow.
*/
resource "aws_dynamodb_table" "this" {
  count          = local.dynamodb_enabled ? 1 : 0
  name           = join("-", [local.name, "meta", var.environment])
  billing_mode   = var.default_dynamodb_billing_mode
  read_capacity  = var.default_dynamodb_read_capacity
  write_capacity = var.default_dynamodb_write_capacity
  hash_key       = "ID"
  range_key      = "Type"
  /*
  NOTES:
    The main idea is to place under ID as uniq identifier to fetch information related to ETL and use
    Type as service/object that is related to
    Example:
      TYPE: StepFunction
      ID: 94E73EDA-53EB-4F87-A990-5421FBC1B278
      ARGS: {"myarg": "myvalue"}

      ID:   SCHEMA_1
      TYPE: REDSHIFT
      ARGS: {"table_list": ["table_1", "table_2"]}

      ID: StepFunctionName
      TYPE: StepFunction
      DEFAULT_ARGS: ....
  */
  attribute {
    name = "ID"
    type = "S"
  }

  attribute {
    name = "Type"
    type = "S"
  }

  tags = local.effective_tags
}


