output "name" {
  value = local.name
}
output "bucket" {
  value = aws_s3_bucket.this
}

output "dynamodb" {
  value = aws_dynamodb_table.this
}

